package StepsDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//import pages.Button_GT;
import pages.loginpage_GT;

public class LoginDemoSteps_GT {
	
	WebDriver driver=null;
	loginpage_GT login;

	
	@Given("browser is open")
	public void browser_is_open() {
		System.out.println("====I am inside LoginDemoSteps_Germany Telekom Live Environment====");
		
				System.out.println("Inside Step-Browser is open");
	
			String projectPath =System.getProperty("user.dir");
				System.out.println("Project path is : "+ projectPath);
		
				System.setProperty("webdriver.chrome.driver",projectPath +"/src/test/resources/drivers/chromedriver.exe");
				
				
				//DesiredCapabilities handlSSLErr = DesiredCapabilities.chrome ()    ;   
					//	handlSSLErr.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
						//WebDriver driver = new ChromeDriver (handlSSLErr);
				driver = new ChromeDriver();
				
		
			//driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
				//driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
				//driver.manage().window().maximize();
				driver.navigate().to("http://gewinnspiele.telekom.de/telekom-web/#/");	
	}
	//Button_GT consentAcceptAll;
	@And("user click on allow cookies")
	public void user_click_on_allow_cookies() throws InterruptedException {
		
		//driver.manage (). getCookies ();
		//Thread.sleep(4000);
		//consentAcceptAll.kostas();
		  driver.findElement(By.id("consentAcceptAll")).click();
		  
		
	}

	@And("user click on Jetzt mitmachen")
	public void user_click_on_jetzt_mitmachen() throws InterruptedException  {
		
		
		
		driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[2]/div/div/div[4]/div[1]/button")).click();
		//driver.findElement(By.id("consentAcceptAll")).click();
		//Thread.sleep(4000);
		driver.navigate().to("https://accounts.login.idm.telekom.com/oauth2/auth?response_type=code&client_id=10LIVESAM30000004901KCGAMEIFICATION00000&scope=openid&claims=%7B%22id_token%22:%7B%22urn:telekom.com:all%22:%7B%22essential%22:false%7D%7D%7D&state=account&redirect_uri=https://gewinnspiele.telekom.de/telekom-web/%23/authCode\r\n"
				+ "	");
		Thread.sleep(14000);
		}

	@When("^user enters (.*) and (.*)$")
	public void user_enters_username_and_password(String username,String password) throws InterruptedException  {
//		public void user_enters_username_and_password(String username,String password) throws InterruptedException  {
			
					login= new loginpage_GT(driver);
				login.insertUsername(username);
				login.insertPassword(password);
					
//					//driver.findElement(By.id("name")).sendKeys(username);
//					//driver.findElement(By.id("password")).sendKeys(password);
	}

	@And("user clicks on login")
	public void user_clicks_on_login() {
		login.clickLogin();
	}

	@Then("user is navigated to the home page")
	public void user_is_navigated_to_the_home_page() {
	   
	}
	
	
	

}
