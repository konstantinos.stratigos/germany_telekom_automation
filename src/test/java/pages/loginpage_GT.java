package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class loginpage_GT {
	protected WebDriver driver;
	
	
private By txt_username = By.id("pw_usr");
	
	private By txt_password= By.id("pw_pwd");
	
	private By btn_login =By.id("pw_submit");
	
	//private By btn_logout=By.id("logout");
	
	
	public loginpage_GT(WebDriver driver)
	{
		this.driver=driver;
		
		
		
		if (!driver.getTitle().equals("Telekom Login")) {
			
			throw new IllegalStateException("This is not Login Page.The Current page is"
					+driver.getCurrentUrl());
			
		}
	
}
	
	
	public void insertUsername(String username) {
		
	driver.findElement(txt_username).sendKeys(username);	
		
	}
	
	public void insertPassword(String password) {
		
		driver.findElement(txt_password).sendKeys(password);	
			
		}
	
	public void clickLogin() {
driver.findElement(btn_login).click();
}
	



}


