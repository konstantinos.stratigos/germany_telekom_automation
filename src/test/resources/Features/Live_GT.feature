Feature: Germany Telekom Live (http://gewinnspiele.telekom.de/telekom-web/#/)

  Scenario Outline: Check login is succesful with valid credentials
    Given browser is open
    And user click on allow cookies
    And user click on Jetzt mitmachen
    When user enters <username> and <password>
    And user clicks on login
    Then user is navigated to the home page
   

    Examples: 
      | username  | password |
      | Gamification@t-online.de | Test1234 |
